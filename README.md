# Tests unitaires | Projet Final | FRONT
##### Auteurs : [Enzo Avagliano](https://github.com/eloxfire) & [Alexandre Brun Giglio](https://github.com/AlexBrunGiglio)


## Getting started
- Clonez le projet avec `git clone https://gitlab.com/ynov-cours/m1-test-unitaires/tests-unitaires-front.git`
- Installez les dépendences avec `npm i`
- Lancez le serveur avec `npm start`
- Rendez vous sur `http://localhost:3000`.

Voilà, votre projet est configuré

## Running the tests
- Utilisez `npm run test` pour effectuer les tests du projet

## Available routes :
Le projet comporte du routing. En voici la map :

- /
  - Page d'accueil / catalogue du projet
- /products/:id
  - Accède a la page spécifique d'un produit
- /cart
  - Affiche le panier en cours

import { render, screen } from '@testing-library/react';
import Enzyme, { mount, shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import App from './App';
import Navbar from './components/Navbar';
import Product from './components/Product';

Enzyme.configure({ adapter: new Adapter() });


// Render testings
test('Render App without issue', () => {
  render(<App />);
});

test('Render Navbar without issue', () => {
  render(<Navbar />);
  const shopLink = screen.getByText("Magasin");
  const cartLink = screen.getByText("Panier");
  expect(shopLink).toBeInTheDocument();
  expect(cartLink).toBeInTheDocument();

  expect(shopLink).toBeInstanceOf(HTMLAnchorElement);
  expect(cartLink).toBeInstanceOf(HTMLAnchorElement);
});

test('Render products cards according to data', () => {
  const wrapper = shallow(<App />);

  // Need to set a timeout because data is not there at render
  //This call is too fast compared to the data retrieve otherwise
  setTimeout(() => {
    wrapper.update();
    expect(wrapper.find(<Product />).length).toEqual(20);
  })
});

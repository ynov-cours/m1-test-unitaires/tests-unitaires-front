import React from 'react'

export default function Product({ image, name, gender, species, type, status, price, id }) {
  return (
    <div className="product col-2 m-3">
      <img className="product-image" src={image} alt={name} />
      <div className="p-2">
        <h5 className="product-title">{name}</h5>
        <hr className="product-separator" />
        <p className="product-text mb-0">Détails :</p>
        <ul>
          <li>Genre : {gender}</li>
          <li>Espèce : {species}</li>
          <li>Type : {type !== "" ? type : "N/A"}</li>
          <li>Status : {status}</li>
        </ul>
        <hr className="product-separator" />
        <div className="d-flex flex-row justify-content-between align-items-center mb-3 px-2">
          <h5 className="product-title">Prix : {price}€</h5>
          <a className="product-button p-2" href={`/product/${id}`}>Voir le produit</a>
        </div>
      </div>
    </div>
  )
}

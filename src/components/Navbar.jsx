import React from 'react'
import '../sass/home.scss';

export default function Navbar() {
  return (
    <div className="navbar d-flex flex-row align-items-center justify-content-start px-5">
      <p className="navbar-title me-5 m-0">Rick et Morty Shop</p>
      <a className="navbar-link me-5" href="/">Magasin</a>
      <a className="navbar-link me-5" href="/cart">Panier</a>
    </div>
  )
}

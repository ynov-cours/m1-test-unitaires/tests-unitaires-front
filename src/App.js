import { BrowserRouter as Router, Routes as Switch, Route } from 'react-router-dom';

import Navbar from './components/Navbar';
import CartPage from './pages/CartPage';
import HomePage from './pages/HomePage';
import NotFound from './pages/NotFound';
import ProductPage from './pages/ProductPage';

function App() {
  return (
    <div className="App">
      <Router>
        <Navbar />
        <Switch>
          <Route path="/" element={<HomePage />} />
          <Route path="/product/:id" element={<ProductPage />} />
          <Route path="/cart" element={<CartPage />} />
          <Route path="*" element={<NotFound />} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;

import axios from 'axios';
import React, { useState, useEffect } from 'react'
import Product from '../components/Product';
import '../sass/home.scss';

export default function HomePage() {

  const [products, setProducts] = useState([]);

  const getProducts = async () => {
    try {
      axios.get('http://localhost:8000/products/getAll')
        .then(response => {
          setProducts(response.data);
        })
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <div id="home">
      <div className="col-12 d-flex flex-wrap justify-content-around align-items-center content px-5">
        {
          products.length > 0
            ?
            products.map((item, index) => {
              return (
                <Product
                  id={item.id}
                  name={item.name}
                  gender={item.gender}
                  image={item.image}
                  price={item.price}
                  species={item.species}
                  status={item.status}
                  type={item.type}
                  key={`product-${index}`}
                />
              )
            })
            :
            <h1>Aucun produit trouvé...</h1>
        }
      </div>
    </div>
  )
}
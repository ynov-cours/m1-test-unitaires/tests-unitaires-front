import axios from 'axios';
import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom';
import '../sass/product.scss';

export default function ProductPage() {

  let params = useParams();
  const [product, setProduct] = useState([]);
  const [quantity, setQuantity] = useState(1);

  const getProductsById = async (id) => {
    try {
      axios.get(`http://localhost:8000/products/get/${id}`)
        .then(response => {
          setProduct(response.data[0]);
        })
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getProductsById(params.id)
  }, []);

  const addProductToCart = () => {

    const newProduct = {
      id: product.id,
      name: product.name,
      quantity: quantity,
      price: product.price,
      image: product.image,
    }

    axios.post(`http://localhost:8000/cart/add/`, newProduct)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  }


  return (
    <div id="productPage" className="d-flex flex-row justify-content-center align-items-center">
      <img src={product.image ? product.image : ""} alt={product.name} />
      <div className="ms-5">
        <a href='/' className="white mb-5 link">&#x0003C; Retour</a>
        <h1 className="white">{product.name}</h1>
        <ul>
          <li className="white">Gender : {product.gender}</li>
          <li className="white">Status : {product.status}</li>
          <li className="white">Species : {product.species}</li>
          <li className="white">Type : {product.type === "" ? "N/A" : product.type}</li>
        </ul>
        <h3 className="white">Prix : {product.price}€</h3>
        <div className="d-flex flex-row justify-content-between">
          <div>
            <button onClick={() => addProductToCart()} className="addToCartButton p-2">Ajouter au panier</button>
          </div>
          <div className="d-flex flex-row align-items-center ms-4">
            <p className="mb-0 me-2">Quantité :</p>
            <input className="col-2" type="number" placeholder="1" onChange={(e) => setQuantity(e.target.value)} />
          </div>
        </div>
      </div>
    </div>
  )
}

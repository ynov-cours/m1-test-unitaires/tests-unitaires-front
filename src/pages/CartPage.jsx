import axios from 'axios';
import React, { useState, useEffect, Fragment } from 'react'
import '../sass/home.scss';

export default function CartPage() {

  const [cart, setCart] = useState([]);
  const [cartSubtotal, setCartSubtotal] = useState();
  const [hookTrigger, setHookTrigger] = useState();

  const randomizeLetters = () => {
    const alphabet = "abcdefghijklmnopqrstuvwxyz";
    const randomCharacter = alphabet[Math.floor(Math.random() * alphabet.length)];
    const randomCharacter2 = alphabet[Math.floor(Math.random() * alphabet.length)];

    return `${randomCharacter}-${randomCharacter2}`;
  }


  const getProducts = async () => {
    try {
      axios.get('http://localhost:8000/cart/get')
        .then(response => {
          setCart(response.data);
        })
    } catch (error) {
      console.log(error);
    }
  }

  const getCartSubtotal = async () => {
    try {
      axios.get('http://localhost:8000/cart/get/subtotal')
        .then(response => {
          setCartSubtotal(response.data);
        })
    } catch (error) {
      console.log(error);
    }
  }


  useEffect(() => {
    getProducts();
    getCartSubtotal();
  }, [hookTrigger]);


  const deleteItem = (id) => {
    try {
      axios.post(`http://localhost:8000/cart/delete/${id}`)
        .then(response => {
          setHookTrigger(randomizeLetters());
        })
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div id="cart">
      <div className="col-12 d-flex flex-column justify-content-center align-items-center content px-5">
        <h1 className="mb-5">Aperçu de votre panier :</h1>
        {
          cart.length > 0
            ?
            <Fragment>
              <table className="tg">
                <thead>
                  <tr>
                    <th className="tg-0lax p-3">Produit</th>
                    <th className="tg-0lax p-3">Quantité</th>
                    <th className="tg-0lax p-3">Prix unitaire</th>
                    <th className="tg-0lax p-3">Prix total</th>
                    <th className="tg-0lax p-3"></th>
                  </tr>
                </thead>
                <tbody>
                  {
                    cart.map((item, index) => {
                      return (
                        <tr>
                          <td className="tg-0lax p-3">{item.product_id} - {item.product_name}</td>
                          <td className="tg-0lax p-3">{item.product_quantity}</td>
                          <td className="tg-0lax p-3">{item.product_price_unit}</td>
                          <td className="tg-0lax p-3">{item.product_price_total}</td>
                          <td onClick={() => deleteItem(item.product_id)} className="tg-0lax p-3 delete-col">X</td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
            </Fragment>
            :
            <h1>Aucun élément trouvé dans votre panier...</h1>
        }

        <h1 className="mt-5">Sous total panier : {cartSubtotal}€</h1>

      </div>
    </div>
  )
}
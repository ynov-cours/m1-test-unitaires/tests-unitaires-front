import React from 'react'
import '../sass/home.scss';

export default function NotFound() {
  return (
    <div id="notfound">
      <div className="col-12 d-flex flex-column justify-content-center align-items-center content px-5">
        <h1 className="mb-5">Cette page n'existe pas</h1>
        <h3><a href="/">Retour au magasin</a></h3>
      </div>
    </div>
  )
}